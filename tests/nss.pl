#!/usr/bin/perl

my $RESULTFILE="result-nss.tmp";

use File::Which qw(which);

print "Checking the NSS configuration\n";

my $dir = 'tests/outputs';

opendir(DIR, $dir) or die $!;

my @nsspolicies
    = grep {
        /-nss/             # has -nss in name
        && -f "$dir/$_"   # and is a file
    } readdir(DIR);

foreach my $policyfile (@nsspolicies) {
	my $policy = $policyfile;
	$policy =~ s/-[^-]+$//;

	print "Checking policy $policy\n";
	my $tool = which "nss-policy-check";

	if ($policy ne 'EMPTY' and $tool ne undef) {

		system("nss-policy-check $dir/$policyfile >$RESULTFILE 2>&1") ;
		if ($? != 0) {
			print "Error in NSS policy for $policy\n";
			print STDERR "NSS policy for $policy:\n";
			system("cat $dir/$policyfile 1>&2");
			print STDERR "\nnss-policy-check error:\n";
			system("cat $RESULTFILE 1>&2");
			exit 1;
		}
		unlink($RESULTFILE);
	}
}

exit 0;
